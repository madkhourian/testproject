package madkhourian.uandme.Com.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import madkhourian.uandme.Com.CustomView.MyTextView;
import madkhourian.uandme.Com.entities.cartModel;
import madkhourian.uandme.R;

public class CartAdapter extends BaseAdapter {
   Context mContext;
    List<cartModel> carts;

    public CartAdapter(Context mContext, List<cartModel> carts) {
        this.mContext = mContext;
        this.carts = carts;
    }

    @Override
    public int getCount() {
        return carts.size();
    }

    @Override
    public Object getItem(int position) {
        return carts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext)
                .inflate(R.layout.carts_list_item,viewGroup,false);

        ImageView ImageCli =  row.findViewById(R.id.ImageCli);
        MyTextView name = row.findViewById(R.id.name);
        MyTextView company = row.findViewById(R.id.company);
        Glide.with(mContext).load(
                carts.get(position).getImageURL()
        ).into(ImageCli) ;

        name.setText(carts.get(position).getName());
        company.setText(carts.get(position).getCompany());
        return row;    }
}
