package madkhourian.uandme.Com.entities;

public class cartModel {
    String name,company,imageURL;

    public cartModel(String name, String company, String imageURL) {
        this.name = name;
        this.company = company;
        this.imageURL = imageURL;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
