package madkhourian.uandme.Com.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class PublicMethods {

    public static void toast(Context mContext, String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static Typeface getTypeFace(Context mContext) {
        return Typeface.createFromAsset(
                mContext.getAssets(), "sans.ttf"
        );
    }

    public static void setShared(String key, String value) {
        Hawk.put(key, value);
//        PreferenceManager.getDefaultSharedPreferences(mContext)
//                .edit().putString(key , value)
//                .apply();
    }

    public static String getShared(String key, String defValue) {
        return Hawk.get(key, defValue);
//        return PreferenceManager.getDefaultSharedPreferences(mContext)
//                .getString(key,defValue) ;
    }



}
