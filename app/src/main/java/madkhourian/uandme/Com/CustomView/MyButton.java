package madkhourian.uandme.Com.CustomView;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import madkhourian.uandme.Com.Utils.PublicMethods;

public class MyButton extends AppCompatButton {
    public MyButton(Context context) {
        super(context);
        init(context);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    void init(Context mContext){
        this.setTypeface(
                PublicMethods.getTypeFace(mContext)
        );
    }

}
