package madkhourian.uandme.Com.CustomView;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import  madkhourian.uandme.Com.Utils.PublicMethods;

public class MyEdittext extends AppCompatEditText {
    public MyEdittext(Context context) {
        super(context);
        init(context);
    }

    public MyEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyEdittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    void init(Context mContext){
        this.setTypeface(
                PublicMethods.getTypeFace(mContext)
        );
    }

    public String text(){
        return this.getText().toString() ;
    }
}
