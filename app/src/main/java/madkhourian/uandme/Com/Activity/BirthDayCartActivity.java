package madkhourian.uandme.Com.Activity;

import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import madkhourian.uandme.Com.Adapter.CartAdapter;
import madkhourian.uandme.Com.Utils.BaseActivity;
import madkhourian.uandme.Com.Utils.PublicMethods;
import madkhourian.uandme.Com.entities.cartModel;
import madkhourian.uandme.R;

public class BirthDayCartActivity extends BaseActivity {
    ListView cartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birth_day_cart);
        cartList = findViewById(R.id.cartList);
        getPermissions();




        cartModel pride = new cartModel(
                "Pride", "Saipa", "http://media.irib.ir/assets//radio_slider/20180819080848_9325.png"
        );
        cartModel p206 = new cartModel(
                "خودرو 206", "Iran Khodro", "http://saten.ir/wp-content/uploads/2017/05/206.jpg"
        );
//        cartModel pride = new cartModel(
//                "کارت دعوت یگانه", "شرکت بهسا", "http://rosily.ir/wp-content/uploads/2016/03/%DA%A9%D8%A7%D8%B1%D8%AA-%D8%AF%D8%B9%D9%88%D8%AA-%D8%AA%D9%88%D9%84%D8%AF-7.jpg"
//        );
//        cartModel p206 = new cartModel(
//                "باب اسفنجی", "شرکت بهین", "http://www.parsfestival.com/media/catalog/product/md_bc558_kart_bob.jpg"
//        );
//        cartModel Birthday2 = new cartModel(
//                "خواهران یخی", "شرکت انتشارات", "http://rosily.ir/wp-content/uploads/2016/03/%DA%A9%D8%A7%D8%B1%D8%AA-%D8%AF%D8%B9%D9%88%D8%AA-%D8%AA%D9%88%D9%84%D8%AF-7.jpg"
//        );
//        cartModel Birthday3 = new cartModel(
//                "بچه رئیس", "شرکت دعوتی", "http://media.irib.ir/assets/tv_slider/20180827090827_2440.png"
//        );
        List<cartModel> carts = new ArrayList<>();
        carts.add(pride);
        carts.add(p206);
        carts.add(pride);
        carts.add(p206);
        carts.add(pride);
        carts.add(p206);
        carts.add(pride);
        carts.add(p206);
        carts.add(pride);
        carts.add(p206);
//        carts.add(Birthday2);
//        carts.add(Birthday3);

        CartAdapter adapter = new
                CartAdapter(mContext, carts);
        cartList.setAdapter(adapter);



        cartList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                cartModel clickedModel =
                        (cartModel) adapterView.getItemAtPosition(position);
                PublicMethods.toast(mContext , clickedModel.getName());

            }
        });
    }

    private void getPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.INTERNET,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).check();
    }
}
